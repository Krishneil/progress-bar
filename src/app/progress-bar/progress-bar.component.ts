// progress-bar.component.ts
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  templateUrl: 'progress-bar.component.html',
  styleUrls: ['progress-bar.component.scss'],
})
export class ProgressBarComponent {
  @Input() current: number = 10000000;
  @Input() total: number = 35000000;

  get formattedTotal(): string {
    return this.total.toLocaleString();
  }

  get percentage(): number {
    return +(this.current / this.total * 100).toFixed(2); // Round to 2 decimal places
  }
}
